<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class CastController extends Controller
{
    public function create(){
        return view('casts.create');
    }

    public function store(Request $request){ 
        //dd($request->all());
        $request->validate([
            "nama" => 'required|unique:casts',
            "umur" => 'required',
            "bio" => 'required'
        ]);

        $query = DB::table('casts')->insert([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/casts')->with('success', 'Data Berhasil Tersimpan');
    }

    public function index(){
        $tampung = DB::table('casts')->get();
        
        //dd($tampung);
        
        return view('casts.index',compact('tampung'));
    }
    public function show($id){
        $ditampung = DB::table('casts')->where('id', $id)->first();
        
        //dd($ditampung);
        
        return view('casts.show', compact('ditampung'));
    }

    public function edit($id){
        $ditampung = DB::table('casts')->where('id', $id)->first();
        
        //dd($ditampung);
        
        return view('casts.edit', compact('ditampung'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:casts',
            "umur" => 'required',
            "bio" => 'required'
        ]);

        $query = DB::table('casts')
                 ->where('id', $id)
                 ->update([
                     'nama' => $request['nama'],
                     'umur' => $request['umur'],
                     'bio' => $request['bio'],
                 ]);
        
        return redirect('/casts')->with('success', 'Data Berhasil Diupdate');
    }

    public function destroy($id){
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts')->with('success', 'Data Berhasil Didelete');
    }
}
