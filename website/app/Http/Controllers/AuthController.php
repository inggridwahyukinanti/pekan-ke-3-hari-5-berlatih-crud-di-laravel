<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function registrasi(){
       return view('register');
   }

   public function form(Request $request){
    $namadepan = $request->firstnama;
    $namabelakang = $request->lastname;

    //dd($namadepan , $namabelakang); --> hampir sama seperti var_dump yang intinya untuk mengecek apakah inputan itu berhasil masuk

    return view('welcome', compact('namadepan','namabelakang'));
}
}
