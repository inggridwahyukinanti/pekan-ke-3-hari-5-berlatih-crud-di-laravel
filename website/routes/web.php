<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/master', function () {
    return view('adminlte.master'); //karena file master.blade.php berada di folder adminlte, jika master.blade.phpnya di folder views saja maka cukup ketik master saja
});

Route::get('/tableku', function () {
    return view('tableku.table');
});

Route::get('/tableku/tabeldatabase', function () {
    return view('tableku.tabeldatabase');
});

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@registrasi');

Route::post('/welcome', 'AuthController@form');
*/

Route::get('/casts/create', 'CastController@create');
Route::post('/casts', 'CastController@store');
Route::get('/casts', 'CastController@index');
Route::get('/casts/{id}', 'CastController@show');
Route::get('/casts/{id}/edit', 'CastController@edit');
Route::put('/casts/{id}', 'CastController@update'); //karena pada file edit.blade.php akan ditambahkan vitur update yang dimana method yang digunakan bukan lagi POST melainkan PUT dengan cara menggunakan direktif @method('PUT') maka pada pada web.php ini ditambahkan Route::put('/casts/{id}', 'CastController@update')
Route::delete('/casts/{id}', 'CastController@destroy');