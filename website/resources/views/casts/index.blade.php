@extends('adminlte.master')

@section('content')

<div class='mt-4 ml-4'>

<div class="card">
              <div class="card-header">
                <h3 class="card-title">List Data Pemain Film</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>  
              @endif
              <a class="btn btn-info mb-3" href="/casts/create"> Create New Cast </a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($tampung as $key => $tampung)
                    <tr>
                        <td> {{ $key +1 }} </td>
                        <td> {{ $tampung->nama }} </td>
                        <td> {{ $tampung->umur }} </td>
                        <td> {{ $tampung->bio }} </td>

                        <td style="display: flex;"> <!--Konten-konten yang ada didalam nya akan disusun secara difault yaitu dari kiri ke kanan -->
                        <a href="/casts/{{$tampung->id}}" class="btn btn-info btn-sm"> Show </a>
                        <a href="/casts/{{$tampung->id}}/edit" class="btn btn-warning btn-sm"> Edit </a> <!-- ini untuk membuat tombol edit -->
                        <form action="/casts/{{$tampung->id}}" method="post"> 
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>

                        </td>
                    </tr>
                    @empty  
                    
                    <tr>
                        <td colspan="4" align="center"> No Cast </td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            

</div>
@endsection