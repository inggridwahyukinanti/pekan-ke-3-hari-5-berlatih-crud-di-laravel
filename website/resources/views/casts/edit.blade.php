@extends('adminlte.master')

@section('content')
<div class="ml-4 mt-4">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Post</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/casts/{{$ditampung->id}}" method="POST">
              @csrf
              <!-- Biasanya untuk edit dia akan menggunakan PUT atau PATCH , tapi karena di form laravel itu hanya ada POST jadi kita akan menggunakan atau kasih direktif @method -->
              @method('PUT') 
              
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Name</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $ditampung->nama ) }}" placeholder="Ketikan Nama" required>
                    @error('nama')
                     <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{ old('umur', $ditampung->umur ) }}" placeholder="Umur">
                    @error('umur')
                     <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('nama', $ditampung->bio ) }}" placeholder="Ketikan Bio" require>
                    @error('bio')
                     <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
        </div>
@endsection